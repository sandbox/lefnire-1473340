<?php
/**
 * @file
 * debut_resource.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function debut_resource_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'resource-content-type';
  $context->description = '';
  $context->tag = 'Content types';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'resource' => 'resource',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-resources-block_3' => array(
          'module' => 'views',
          'delta' => 'resources-block_3',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-resources-block_2' => array(
          'module' => 'views',
          'delta' => 'resources-block_2',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content types');
  $export['resource-content-type'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'resources';
  $context->description = '/resources search landing page';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'resources:page' => 'resources:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-resources-block_1' => array(
          'module' => 'views',
          'delta' => 'resources-block_1',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views--exp-resources-page' => array(
          'module' => 'views',
          'delta' => '-exp-resources-page',
          'region' => 'content_header',
          'weight' => '-10',
        ),
        'views-successes-block_1' => array(
          'module' => 'views',
          'delta' => 'successes-block_1',
          'region' => 'content_sidebar',
          'weight' => '-44',
        ),
        'views-nodequeue_1-block' => array(
          'module' => 'views',
          'delta' => 'nodequeue_1-block',
          'region' => 'content_sidebar',
          'weight' => '-43',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('/resources search landing page');
  $export['resources'] = $context;

  return $export;
}
