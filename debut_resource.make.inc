
; Drupal version
core = 7.x
api = 2

; Contrib modules
projects[debut][subdir] = contrib
projects[debut_resource][subdir] = contrib
projects[debut_resource][version] = 1.0-beta1
projects[debut_media][subdir] = contrib
projects[context][subdir] = contrib
projects[date][subdir] = contrib
projects[ctools][subdir] = contrib
projects[features][subdir] = contrib
projects[strongarm][subdir] = contrib
projects[views][subdir] = contrib
