<?php
/**
 * @file
 * debut_resource.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function debut_resource_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'resources';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Resources';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Resources';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = 0;
  $handler->display->display_options['row_options']['default_field_elements'] = 1;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_wrapper_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Date Published */
  $handler->display->display_options['fields']['field_publication_date']['id'] = 'field_publication_date';
  $handler->display->display_options['fields']['field_publication_date']['table'] = 'field_data_field_publication_date';
  $handler->display->display_options['fields']['field_publication_date']['field'] = 'field_publication_date';
  $handler->display->display_options['fields']['field_publication_date']['label'] = '';
  $handler->display->display_options['fields']['field_publication_date']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_publication_date']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_publication_date']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_publication_date']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_publication_date']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_publication_date']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_publication_date']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_publication_date']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_publication_date']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_publication_date']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_publication_date']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_publication_date']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_publication_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_publication_date']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_publication_date']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_publication_date']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_publication_date']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_publication_date']['settings'] = array(
    'format_type' => 'event_day',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => '',
  );
  $handler->display->display_options['fields']['field_publication_date']['field_api_classes'] = 0;
  /* Field: Content: Author */
  $handler->display->display_options['fields']['field_resource_authors']['id'] = 'field_resource_authors';
  $handler->display->display_options['fields']['field_resource_authors']['table'] = 'field_data_field_resource_authors';
  $handler->display->display_options['fields']['field_resource_authors']['field'] = 'field_resource_authors';
  $handler->display->display_options['fields']['field_resource_authors']['label'] = '';
  $handler->display->display_options['fields']['field_resource_authors']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_resource_authors']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_resource_authors']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_resource_authors']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_resource_authors']['hide_empty'] = 1;
  $handler->display->display_options['fields']['field_resource_authors']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_resource_authors']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['field_resource_authors']['settings'] = array(
    'trim_length' => '50',
  );
  $handler->display->display_options['fields']['field_resource_authors']['group_rows'] = 1;
  $handler->display->display_options['fields']['field_resource_authors']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_resource_authors']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['delta_first_last'] = 0;
  $handler->display->display_options['fields']['field_resource_authors']['field_api_classes'] = 0;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['body']['alter']['external'] = 0;
  $handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['body']['alter']['html'] = 0;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['body']['hide_empty'] = 0;
  $handler->display->display_options['fields']['body']['empty_zero'] = 0;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '200',
  );
  $handler->display->display_options['fields']['body']['field_api_classes'] = 0;
  /* Sort criterion: Content: Date Published (field_publication_date) */
  $handler->display->display_options['sorts']['field_publication_date_value']['id'] = 'field_publication_date_value';
  $handler->display->display_options['sorts']['field_publication_date_value']['table'] = 'field_data_field_publication_date';
  $handler->display->display_options['sorts']['field_publication_date_value']['field'] = 'field_publication_date_value';
  $handler->display->display_options['sorts']['field_publication_date_value']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'resource' => 'resource',
  );
  $handler->display->display_options['filters']['type']['group'] = 0;
  /* Filter criterion: Search: Search Terms */
  $handler->display->display_options['filters']['keys']['id'] = 'keys';
  $handler->display->display_options['filters']['keys']['table'] = 'search_index';
  $handler->display->display_options['filters']['keys']['field'] = 'keys';
  $handler->display->display_options['filters']['keys']['group'] = 0;
  $handler->display->display_options['filters']['keys']['exposed'] = TRUE;
  $handler->display->display_options['filters']['keys']['expose']['operator_id'] = 'keys_op';
  $handler->display->display_options['filters']['keys']['expose']['label'] = 'Keywords';
  $handler->display->display_options['filters']['keys']['expose']['operator'] = 'keys_op';
  $handler->display->display_options['filters']['keys']['expose']['identifier'] = 'keys';
  $handler->display->display_options['filters']['keys']['expose']['multiple'] = FALSE;
  /* Filter criterion: Content: Resource Type (field_resource_type) */
  $handler->display->display_options['filters']['field_resource_type_value']['id'] = 'field_resource_type_value';
  $handler->display->display_options['filters']['field_resource_type_value']['table'] = 'field_data_field_resource_type';
  $handler->display->display_options['filters']['field_resource_type_value']['field'] = 'field_resource_type_value';
  $handler->display->display_options['filters']['field_resource_type_value']['group'] = 0;
  $handler->display->display_options['filters']['field_resource_type_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_resource_type_value']['expose']['operator_id'] = 'field_resource_type_value_op';
  $handler->display->display_options['filters']['field_resource_type_value']['expose']['label'] = 'Resource Type';
  $handler->display->display_options['filters']['field_resource_type_value']['expose']['operator'] = 'field_resource_type_value_op';
  $handler->display->display_options['filters']['field_resource_type_value']['expose']['identifier'] = 'field_resource_type_value';
  $handler->display->display_options['filters']['field_resource_type_value']['expose']['remember'] = 1;
  $handler->display->display_options['filters']['field_resource_type_value']['expose']['reduce'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['group_rendered'] = 1;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = 0;
  $handler->display->display_options['row_options']['comments'] = 0;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['path'] = 'resources';

  /* Display: Featured Block */
  $handler = $view->new_display('block', 'Featured Block', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Featured Publication';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';

  $export['resources'] = $view;

  return $export;
}
